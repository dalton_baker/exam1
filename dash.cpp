#include <iostream>
#include "inputProcessor.h"

int main (int argc, char* const argv[])
{
    bool askForInput = true;
    std::string input;
    InputProcessor* inputProcessor = new InputProcessor();

    while(askForInput)
    {
        //std::cout << "dash>";
        std::cout << "\033[1;32mdash>\033[0m";
        std::getline(std::cin, input);

        askForInput = input != "exit";

        if(askForInput && !input.empty())
        {
            inputProcessor->processInput(input);
        }
    }
    
    return 0;
}