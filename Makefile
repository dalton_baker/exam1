SOURCE = dash.cpp inputProcessor.cpp tools.cpp fileProcessor.cpp

OBJS = $(SOURCE:.cpp=.o)

#GNU C/C++ Compiler
GCC = g++
LINK = g++

# Compiler flags
CFLAGS = -Wall -O3 -std=c++11
CXXFLAGS = $(CFLAGS)

# Fill in special libraries needed here
LIBS = -lm -lpthread

.PHONY: clean

all : dash

dash: $(OBJS)
	$(LINK) -o $@ $^ $(LIBS)

clean:
	rm -rf *.o *.d core dash exam1.tgz .vscode

debug: CXXFLAGS = -DDEBUG -g -std=c++11
debug: dash

tar: clean
	tar zcvf exam1.tgz ../exam1/Makefile ../exam1/*.cpp ../exam1/*.h ../exam1/*.pdf

-include $(SOURCE:.cpp=.d)

%.d: %.cpp
	@set -e; /bin/rm -rf $@;$(GCC) -MM $< $(CXXFLAGS) > $@
