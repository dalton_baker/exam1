#include "tools.h"

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will split a string into a list of stirings by the deliminator
 * 
 * @param[in] input - the string to split
 * @param[in] delim - the string to deliminate by
 * 
 * @return - std::list<std::string> list of strings broken by the delim
 ******************************************************************************/
std::list<std::string> splitString(std::string input, const std::string delim)
{
    std::list<std::string> output;
    size_t sub;

    if(input.empty())
    {
        return output;
    }

    while((sub = input.find(delim)) != std::string::npos)
    {
        output.push_back(input.substr(0, sub));
        input.erase(0, sub + delim.length());
    }

    if(input.length() > 0)
    {
        output.push_back(input);
    }

    return output;
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will tell you if string is an int
 * 
 * @param[in] input - the string to check
 * 
 * @return - bool
 ******************************************************************************/
bool isInt(std::string input)
{
    for(char c : input)
    {
        if(!std::isdigit(c))
        {
            return false;
        }
    }

    return true;
}