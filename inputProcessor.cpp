/**************************************************************************/ /**
 * @file
 * @brief This file contains the members of the ProcessInput class.
 *****************************************************************************/
#include "inputProcessor.h"

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * The constructor InputPorcessor
 ******************************************************************************/
InputProcessor::InputProcessor()
{
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will process the users input
 * 
 * @param[in] userInput - the users input
 ******************************************************************************/
void InputProcessor::processInput(std::string userInput)
{
    std::list<std::string> splitInput = splitString(userInput, " ");

    if (splitInput.front() == "cmdnm")
    {
        processCmdnm(splitInput);
    }
    else if (splitInput.front() == "pid")
    {
        processPid(splitInput);
    }
    else if (splitInput.front() == "systat")
    {
        processSystat(splitInput);
    }
    else if (splitInput.front() == "cd")
    {
        processCd(splitInput);
    }
    else
    {
        processCommand(splitInput);
    }
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will process the "cmdnm" command
 * 
 * @param[in] inputList - A list of strings, the users input split by " "
 ******************************************************************************/
void InputProcessor::processCmdnm(std::list<std::string> inputList)
{
    if (inputList.size() != 2 || !isInt(inputList.back()))
    {
        std::cout << "  malformed command:\n    must be \"cmdnm <pid>\" <pid> must be an int\n";
        return;
    }

    if (!fileProcessor.isFolder("/proc/" + inputList.back()))
    {
        std::cout << "  pid does not exist: " << inputList.back() << "\n";
        return;
    }

    for (std::string s : fileProcessor.getFileContents("/proc/" + inputList.back() + "/status"))
    {
        if (s.find("Name:") != std::string::npos)
        {
            std::cout << splitString(s, "\t").back() << "\n";
            return;
        }
    }
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will process the "pid" command
 * 
 * @param[in] inputList - A list of strings, the users input split by " "
 ******************************************************************************/
void InputProcessor::processPid(std::list<std::string> inputList)
{
    if (inputList.size() != 2)
    {
        std::cout << "  malformed command:\n    must be \"pid <command>\"\n";
        return;
    }

    for (std::string dir : fileProcessor.getSubDirectories("/proc/"))
    {
        std::string statusFile = "/proc/" + dir + "/status";
        if (fileProcessor.isFile(statusFile))
        {
            for (std::string s : fileProcessor.getFileContents(statusFile))
            {
                if (s.find("Name:") != std::string::npos &&
                    s.find(inputList.back()) != std::string::npos)
                {
                    std::cout << dir << "\n";
                }
            }
        }
    }
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will process the "systat" command
 * 
 * @param[in] inputList - A list of strings, the users input split by " "
 ******************************************************************************/
void InputProcessor::processSystat(std::list<std::string> inputList)
{
    if (inputList.size() != 1)
    {
        std::cout << "  malformed command:\n    systat does not accept arguments\n";
        return;
    }

    std::cout << "\nversion:\n";
    for (std::string s : fileProcessor.getFileContents("/proc/version"))
    {
        std::cout << s << "\n";
    }

    std::cout << "\nuptime:\n";
    for (std::string s : fileProcessor.getFileContents("/proc/uptime"))
    {
        std::cout << s << "\n";
    }

    std::cout << "\nmeminfo:\n";
    for (std::string s : fileProcessor.getFileContents("/proc/meminfo"))
    {
        std::cout << s << "\n";
    }

    std::cout << "\ncpuinfo:\n";
    for (std::string s : fileProcessor.getFileContents("/proc/cpuinfo"))
    {
        if (s.find("vendor_id") != std::string::npos ||
            s.find("cpu family") != std::string::npos ||
            s.find("model") != std::string::npos ||
            s.find("model name") != std::string::npos ||
            s.find("stepping") != std::string::npos ||
            s.find("cpu MHz") != std::string::npos ||
            s.find("cache size") != std::string::npos)
        {
            std::cout << s << "\n";
        }
    }
    std::cout << "\n";
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will process the "cd" command
 * 
 * @param[in] inputList - A list of strings, the users input split by " "
 ******************************************************************************/
void InputProcessor::processCd(std::list<std::string> inputList)
{
    if (inputList.size() != 2)
    {
        std::cout << "  malformed command:\n    must be \"cd <absolute path>\" or \"cd <absolute path>\"\n";
        return;
    }

    if (!fileProcessor.isFolder(inputList.back()))
    {
        std::cout << inputList.back() << " is not a directory\n";
        return;
    }

    if (chdir(inputList.back().c_str()) == 0)
    {
        std::cout << "Changed to directory " << fileProcessor.getCurrentPath() << "\n";
    }
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will process all commands for bash
 * 
 * @param[in] inputList - A list of strings, the users input split by " "
 ******************************************************************************/
void InputProcessor::processCommand(std::list<std::string> inputList)
{
    int cpid;
    int stat;
    char *args[1024];

    //build char array from string list
    int pos = 0;
    for(std::string s : inputList)
    {
        args[pos] = new char[s.length()];
        strcpy(args[pos], s.c_str());
        pos++;
    }
    args[pos] = NULL;

    //start child process
    cpid = fork();
    if (cpid == 0)
    {
        execvp(args[0], args);
        perror("Command execution failed: ");
        exit(5);
    }
    else
    {
        std::cout << "Chiled process stated. Pid: " << cpid << "\n\n";
    }

    cpid = wait(&stat);

    struct rusage usage;
    getrusage(RUSAGE_CHILDREN, &usage);
    std::cout << "\nStats for child processes";
    std::cout << "\n  User Time:   " << usage.ru_utime.tv_usec << " ms";
    std::cout << "\n  System Time: " << usage.ru_stime.tv_usec << " ms";
    std::cout << "\n  Page Faults: " << usage.ru_majflt;
    std::cout << "\n  Swaps:       " << usage.ru_nswap << "\n\n";
}