/**************************************************************************//**
 * @file
 * @brief This is the header file for tools.cpp
 *****************************************************************************/
#ifndef __TOOLS_H
#define __TOOLS_H

#include <iostream>
#include <list>

std::list<std::string> splitString(std::string, const std::string);
bool isInt(std::string);

#endif
