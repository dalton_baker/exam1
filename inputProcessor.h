/**************************************************************************/ /**
 * @file
 * @brief This is the header file for inputProcessor.cpp
 *****************************************************************************/
#ifndef __PROCESSINPUT_H
#define __PROCESSINPUT_H

#include "tools.h"
#include "fileProcessor.h"
#include <iostream>
#include <string.h>
#include <sys/wait.h>
#include <sys/resource.h>

/*!
* @brief This will process input from the user
*/
class InputProcessor
{
    FileProcessor fileProcessor;

public:
    InputProcessor();
    void processInput(std::string);

private:
    void processCmdnm(std::list<std::string>);
    void processPid(std::list<std::string>);
    void processSystat(std::list<std::string>);
    void processCd(std::list<std::string>);
    void processCommand(std::list<std::string>);
};

#endif
